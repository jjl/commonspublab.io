---
title: People and Projets around CommonsPub
subtitle: Implementations, plugins & modules, client apps, etc
comments: false
---

The first projects to use CommonsPub and/or contribute to the project are:

* [MoodleNet](https://new.moodle.net/) (social network to empower communities of educators to connect, learn, and curate open content together) has contributed a lot to CommonsPub but has now forked it to focus on its own roadmap.

* [Open Cooperative Ecosystem](https://opencoopecosystem.net/) to empower economic activities driven by human and ecological needs rather than profit.

We ([ZO Team](https://zo.team/)) are looking for others who want to participate. 