---
title: Why CommonsPub
subtitle: Why this project exists
comments: false
---


We want to foster an open source federated app ecosystem, using the ActivityPub W3C standard.

ActivityPub gives applications a shared protocol and syntax that they can use to communicate with each other. If a server implements ActivityPub, it can publish posts that any other server that implements ActivityPub knows how to share, like and reply to. The vocabulary (ActivityStreams) can be extended to represent other types of activities beyond the initial social ones, including economic and political activities.

So far writing software that implements federation logic and data structues has been technically challenging, even when attempting to implement a very simple app.

CommonsPub wants to give developers the ability to focus on the specific logic of their application by providing a kit that manages the data and federation out of the box.

Developers could enable users within the network to share and receive any kind of activities that can be represented digitally, for example: 
* Posting text, links, rich articles, images, videos, and other documents
* Group discussion and collaboration
* Private messaging
* Organising events/meet-ups
* Group decision-making tools 
* Crowdfunding 
* Federated version control (eg. wikis or code)
* Location-based posts (eg. couchsurfing, carpooling)
* Skill sharing (eg. time banking)
* Resource sharing (eg. virtual tool libraries)
* Local trade (eg. art and crafts, second hand)

CommonsPub should also enable end users to deploy a server and federate with other instances on the web that share the same protocols, like Mastodon, Pleroma, GNU Social, Diaspora, Peertube, etc - synchronising any kind of data and activities in a p2p environment, without any central authority.

This is important because it would enable:

- federation between people and communities who do similar things
- federation between apps who do different things
- users to have one identity (and one inbox/outbox) across many apps and communities
- increased modularity and interoperability between data schemas, federation, application logic and user interfaces.

CommonsPub is also a way to promote diversity in decentralization by allowing developers and groups to create apps tailored to their specific needs, or to fork existing apps in order to change their social, political and economical features and algorithms.

