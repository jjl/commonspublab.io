---
title: What’s next for CommonsPub?
date: 2019-03-05
tags: ["CommonsPub", "story"]
---

Since [Moodle has stopped developing CommonsPub](https://wp.me/p9taZW-aY) (and is forking it in order to focus on implementing federation and its own functionality), I would like to emphasise that CommonsPub still exists, but is now fully in hands of the free software community.

There’s a small group of us still dedicated to keeping the project alive and we plan to continue developing it in our spare time, especially refocusing on making it generic and extensible, because we have several federated projects we want to build with it in the future. Unfortunately, none of us are experienced Elixir developers (though we’re learning!). So here’s an open invitation for contributors: we need your help! 

Right now, CommonsPub is basically a generic ActivityStreams-based ORM (ActivityPub’s data format), with some non-generic GraphQL client API endpoints (though we have a draft generic GraphQL schema mapped out), and with support for ActivityPub (the federation protocol) yet to be re-implemented. Short term, next steps for the project include: 

- Clean up MoodleNet-specific code from CommonsPub 

- Structure CommonsPub into a framework or library (or as a starter project with a plugin architecture) to make it easily usable in other projects and extensible with custom functionality, without generic and custom code being all mixed together. 

- Copy useful bits of code from other federated projects written in Elixir, like Pleroma, Mobilizon, and of course from the ongoing developments of MoodleNet’s fork of CommonsPub. The first priority being to get federation working. 

- Add generic GraphQL endpoints to could enable client app developers to implement simple federated apps using an ActivityStreams-based syntax, without having to make any changes to the backend.
