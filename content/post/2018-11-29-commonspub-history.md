---
title: What are the origins of CommonsPub?
subtitle: CommonsPub is a young project that has evolved out of several initiatives born with the intention of creating an open app ecosystem.
date: 2018-12-01
tags: ["CommonsPub", "story"]
---

After discussions on [Loomio] (https://loomio.org/g/exAKrBUp/open-app-ecosystem), [Github] (https://github.com/open-app/) and elsewhere, several communities embraced the idea of ​​developing this type of infrastructure, such as FairCoop and Mutual Aid Network among others.

From these developments and synergies, the [open cooperative ecosystem](https://docs.opencoopecosystem.net/) project was formed to combine the efforts of the different communities and develop compatible software together.

In the summer, given our experiences using protocol as part of the fediverse, and seeing the growing developer community around it, we decided to adopt ActivityPub as a federation protocol.

Shortly after that, a team at [Moodle](https://new.moodle.net/) started developing this federated generic server, by forking [Pleroma](https://git.pleroma.social/pleroma/pleroma) with the intention turning it into a framework/library, to become a generic ActivityPub server that can power many different apps and use cases, all of them as interoperable as possible with each other, and any other ActivityPub-based fediverse app like Mastodon.
