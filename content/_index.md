CommonsPub-based apps and instances can each have their own mix of functionality and user experience, to meet the needs of their users and communities - without isolating themselves in silos but rather promoting interconnections among diverse communities within the broader fediverse network.

## Federated

Each instance can interact with other apps that use the same protocol(s) and users can connect with any user that is part of the extended network (aka fediverse).

CommonsPub provides [ActivityPub](http://activitypub.rocks/) federation, common data types, and identity (for individuals and groups) out of the box, enabling:

- **Communities** to co-design new features to fulfill specific needs of each community, and leverage a set of extensions already developed for the rest of the network. 

- **Developers** to create federated apps faster by focusing on adding application-specific data types, logic and user experience.

## Experimental

CommonsPub is an experiment to facilitate the collective construction of complex networks that guarantee radical personalization.

We are slowly building it to be able to make networks that act according to the vision of the community that uses it and shapes it. 

Algorithms, user experience, features, interfaces, governance, even fonts and colors - every aspect of the social network must be able to be co-designed, remixed and redefined by its users.

## State-of-the-art

The back-end is in [Elixir](https://elixir-lang.org/), using the [Phoenix](https://www.phoenixframework.org/) web framework, running on the [Erlang VM](https://www.erlang.org/), which helps it have a low footprint and be highly performant. The client API uses [GraphQL](https://graphql.org/). The default front-end is in [React.js](https://reactjs.org/) and [Typescript](https://www.typescriptlang.org/). 


Read more about [the purpose of this project](/page/about/), and [what apps are using it?](/page/apps/)
